<?php

declare(strict_types=1);

namespace Drupal\tecla;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DrupalKernelInterface;

/**
 * Deregisters unnecessarily discovered PSR4 class maps of disabled extensions.
 */
class TeclaServiceProvider implements ServiceModifierInterface {

  /**
   * Class loader prefix directories of Drupal core which should be kept.
   *
   * @const array
   */
  const CORE_PSR4_PREFIX_DIRS = [
    'Drupal\\Core\\',
    'Drupal\\Driver\\',
    'Drupal\\Component\\',
  ];

  /**
   * Deregister unnecessarily discovered PSR4 class maps of disabled extensions.
   *
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   *   The container builder.
   *
   * @see \Drupal\Core\Test\TestDiscovery::registerTestNamespaces()
   */
  public function alter(ContainerBuilder $container): void {
    // Fail hard if Tecla is installed on a non-test environment.
    if (!drupal_valid_test_ua()) {
      throw new \LogicException("Tecla should be used only on testing");
    }

    $kernel = $container->get('kernel');
    assert($kernel instanceof DrupalKernelInterface);
    $modules = $container->getParameter('container.modules');
    $kernel_reflection = new \ReflectionClass($kernel);
    $class_loader_prop = $kernel_reflection->getProperty('classLoader');
    $class_loader_prop->setAccessible(TRUE);
    $class_loader = $class_loader_prop->getValue($kernel);
    $class_loader_reflection = new \ReflectionClass($class_loader);
    $prefix_dirs_prop = $class_loader_reflection->getProperty('prefixDirsPsr4');
    $prefix_dirs_prop->setAccessible(TRUE);
    $prefix_dirs = $prefix_dirs_prop->getValue($class_loader);
    foreach ($prefix_dirs as $prefix => $dirs) {
      if (strpos($prefix, 'Drupal\\') !== 0) {
        continue;
      }

      if (in_array($prefix, static::CORE_PSR4_PREFIX_DIRS)) {
        continue;
      }

      $prefix_parts = array_filter(explode('\\', $prefix));
      $prefix_module = count($prefix_parts) === 2
        ? explode('\\', $prefix)[1]
        : NULL;
      if (!$prefix_module || in_array($prefix_module, array_keys($modules), TRUE)) {
        continue;
      }

      unset($prefix_dirs[$prefix]);
    }

    $prefix_dirs_prop->setValue($class_loader, $prefix_dirs);
    $class_loader_prop->setValue($kernel, $class_loader);
  }

}
