INTRODUCTION
------------

Tecla is a very simple PHPUnit test helper tool. It provides a workaround for
unnecessarily registered classes of disabled/uninstalled extensions which
happens if you are running PHPUnit tests.

The workaround is pretty simple: it is driven by a service provider which
removes the appropriate PSR4 mappings from Drupal's class loader. The solution
uses reflectors, so it has some performance impact, but the benefit is a more
realistic behavior during testing.


REQUIREMENTS
------------

There are no requirements.


INSTALLATION
------------

You can install Tecla as you would normally install a contributed Drupal module.


CONFIGURATION
-------------

This module does not have any configuration option.


USAGE
-----

Just enable it in your PHPUnit test class.


DOCUMENTATION
-------------

There is no extra documentation, but I'm pretty sure it is not necessary at all.


MAINTAINERS
-----------

* Zoltán Horváth (huzooka) - https://www.drupal.org/u/huzooka

This project has been sponsored by [Kibit Solutions][1].

[1]: https://kibitsolutions.com/
