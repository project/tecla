<?php

namespace Drupal\Tests\tecla\Kernel;

use Composer\Autoload\ClassLoader;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\migrate_drupal\MigrationConfigurationTrait;

/**
 * Tests class map cleanup.
 *
 * @group tecla
 */
class TeclaTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
  ];

  /**
   * Tests core without class map cleanup.
   */
  public function testWithoutClassCleanup(): void {
    $class_loader = $this->assertBasics();

    $this->assertTrue(
      $class_loader->loadClass(MigrationConfigurationTrait::class),
      "The current Drupal core version does not have the issue anymore."
    );
  }

  /**
   * Tests core with class map cleanup performed by Tecla.
   */
  public function testClassCleanup(): void {
    $module_installer = \Drupal::service('module_installer');
    assert($module_installer instanceof ModuleInstallerInterface);
    $module_installer->install(['tecla']);

    $class_loader = $this->assertBasics();

    $this->assertNull(
      $class_loader->loadClass(MigrationConfigurationTrait::class),
      "Tecla's class cleanup failed."
    );
  }

  /**
   * Performs some fundamental assertions, then returns the actual class loader.
   *
   * @return \Composer\Autoload\ClassLoader
   *   The current class loader instance.
   */
  protected function assertBasics(): ClassLoader {
    $container = \Drupal::getContainer();
    $modules = $container->getParameter('container.modules');

    $this->assertTrue(array_key_exists('system', $modules));
    $this->assertFalse(array_key_exists('migrate_drupal', $modules));

    $kernel = $container->get('kernel');
    $kernel_reflection = new \ReflectionClass($kernel);
    $class_loader_prop = $kernel_reflection->getProperty('classLoader');
    $class_loader_prop->setAccessible(TRUE);
    $class_loader = $class_loader_prop->getValue($kernel);
    $this->assertInstanceOf(ClassLoader::class, $class_loader);

    return $class_loader;
  }

}
